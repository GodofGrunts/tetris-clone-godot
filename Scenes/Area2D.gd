extends Area2D


func _ready():
	var GameNode = get_node("/root/Game")
	self.connect("area_entered", GameNode, "_Area_Entered", [self])
	self.connect("area_exited",  GameNode, "_Area_Exited",  [self])
	pass

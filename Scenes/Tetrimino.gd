extends Node

var scene = preload("res://Scenes/Tetrimino.tscn").instance()
var current_block
var next_block
var next_ref_loc = Vector2(432,48)
onready var IPiece = $Tetrimino/IPiece #0
onready var JPiece = $Tetrimino/JPiece #1
onready var LPiece = $Tetrimino/LPiece #2
onready var OPiece = $Tetrimino/OPiece #3
onready var SPiece = $Tetrimino/SPiece #4
onready var TPiece = $Tetrimino/TPiece #5
onready var ZPiece = $Tetrimino/ZPiece #6

var bag = ["IPiece","JPiece","LPiece","OPiece","SPiece","TPiece","ZPiece"]
var next_bag = []
var new_bag = []

var row1 = {"R1C1": "", "R1C2": "", "R1C3": "", "R1C4": "", "R1C5": "", "R1C6": "", "R1C7": "", "R1C8": "", "R1C9": "", "R1C10": ""}
var row2 = {"R2C1": "", "R2C2": "", "R2C3": "", "R2C4": "", "R2C5": "", "R2C6": "", "R2C7": "", "R2C8": "", "R2C9": "", "R2C10": ""}
var row3 = {"R3C1": "", "R3C2": "", "R3C3": "", "R3C4": "", "R3C5": "", "R3C6": "", "R3C7": "", "R3C8": "", "R3C9": "", "R3C10": ""}
var row4 = {"R4C1": "", "R4C2": "", "R4C3": "", "R4C4": "", "R4C5": "", "R4C6": "", "R4C7": "", "R4C8": "", "R4C9": "", "R4C10": ""}
var row5 = {"R5C1": "", "R5C2": "", "R5C3": "", "R5C4": "", "R5C5": "", "R5C6": "", "R5C7": "", "R5C8": "", "R5C9": "", "R5C10": ""}
var row6 = {"R6C1": "", "R6C2": "", "R6C3": "", "R6C4": "", "R6C5": "", "R6C6": "", "R6C7": "", "R6C8": "", "R6C9": "", "R6C10": ""}
var row7 = {"R7C1": "", "R7C2": "", "R7C3": "", "R7C4": "", "R7C5": "", "R7C6": "", "R7C7": "", "R7C8": "", "R7C9": "", "R7C10": ""}
var row8 = {"R8C1": "", "R8C2": "", "R8C3": "", "R8C4": "", "R8C5": "", "R8C6": "", "R8C7": "", "R8C8": "", "R8C9": "", "R8C10": ""}
var row9 = {"R9C1": "", "R9C2": "", "R9C3": "", "R9C4": "", "R9C5": "", "R9C6": "", "R9C7": "", "R9C8": "", "R9C9": "", "R9C10": ""}
var row10 = {"R10C1": "", "R10C2": "", "R10C3": "", "R10C4": "", "R10C5": "", "R10C6": "", "R10C7": "", "R10C8": "", "R10C9": "", "R10C10": ""}
var row11 = {"R11C1": "", "R11C2": "", "R11C3": "", "R11C4": "", "R11C5": "", "R11C6": "", "R11C7": "", "R11C8": "", "R11C9": "", "R11C10": ""}
var row12 = {"R12C1": "", "R12C2": "", "R12C3": "", "R12C4": "", "R12C5": "", "R12C6": "", "R12C7": "", "R12C8": "", "R12C9": "", "R12C10": ""}
var row13 = {"R13C1": "", "R13C2": "", "R13C3": "", "R13C4": "", "R13C5": "", "R13C6": "", "R13C7": "", "R13C8": "", "R13C9": "", "R13C10": ""}
var row14 = {"R14C1": "", "R14C2": "", "R14C3": "", "R14C4": "", "R14C5": "", "R14C6": "", "R14C7": "", "R14C8": "", "R14C9": "", "R14C10": ""}
var row15 = {"R15C1": "", "R15C2": "", "R15C3": "", "R15C4": "", "R15C5": "", "R15C6": "", "R15C7": "", "R15C8": "", "R15C9": "", "R15C10": ""}
var row16 = {"R16C1": "", "R16C2": "", "R16C3": "", "R16C4": "", "R16C5": "", "R16C6": "", "R16C7": "", "R16C8": "", "R16C9": "", "R16C10": ""}
var row17 = {"R17C1": "", "R17C2": "", "R17C3": "", "R17C4": "", "R17C5": "", "R17C6": "", "R17C7": "", "R17C8": "", "R17C9": "", "R17C10": ""}
var row18 = {"R18C1": "", "R18C2": "", "R18C3": "", "R18C4": "", "R18C5": "", "R18C6": "", "R18C7": "", "R18C8": "", "R18C9": "", "R18C10": ""}
var row19 = {"R19C1": "", "R19C2": "", "R19C3": "", "R19C4": "", "R19C5": "", "R19C6": "", "R19C7": "", "R19C8": "", "R19C9": "", "R19C10": ""}
var row20 = {"R20C1": "", "R20C2": "", "R20C3": "", "R20C4": "", "R20C5": "", "R20C6": "", "R20C7": "", "R20C8": "", "R20C9": "", "R20C10": ""}


func _ready():	
	#bag_randomizer()
	bag_randomizer_debug()
	spawn_block(new_bag)

func _process(delta):
	control_block(current_block)
	next_block_sprite(new_bag)

func bag_randomizer():
	randomize()
	var x = 0
	new_bag = []
	while(x < 7):
		var num = randi() % 7
		if !(new_bag.has(num)):
			new_bag.append(num)
			x += 1

func bag_randomizer_debug():
	new_bag = [0,0,0,0,0,0,0]

func spawn_block(tempbag):
	var num = tempbag[0]
	var piece_name = bag[num]
	var block = scene.get_node(piece_name).duplicate()
	current_block = block
	add_child(block)
	current_block.position=Vector2((112),(-48))
	current_block.set_visible(true)

func control_block(block):
	if Input.is_action_just_pressed("left"):
		block.position += Vector2(-32,0)
	if Input.is_action_just_pressed("right"):
		block.position += Vector2(32,0)
	if Input.is_action_just_pressed("down"):
		block.position += Vector2(0,32)
	if Input.is_action_just_pressed("flip") and block.name != "OPiece":
		block.rotation_degrees += 90
	if Input.is_action_just_pressed("ui_accept"):
		new_bag.pop_front()
		if new_bag.size() == 0:
			#bag_randomizer()
			bag_randomizer_debug()
		spawn_block(new_bag)

func next_block_sprite(tempbag):
	if next_block != null:
		next_block.set_visible(false)
	if tempbag.size() >= 2:
		var num = tempbag[1]
		var piece_name = bag[num]
		var block = get_node("Tetrimino/" + piece_name)
		next_block = block
		if next_block.name == "IPiece":
			next_block.position = next_ref_loc + Vector2(-16,80)
		if next_block.name == "JPiece":
			next_block.position = next_ref_loc + Vector2(-16,48)
		if next_block.name == "LPiece":
			next_block.position = next_ref_loc + Vector2(-16,80)
		if next_block.name == "OPiece":
			next_block.position = next_ref_loc + Vector2(-16,80)
		if next_block.name == "SPiece":
			next_block.position = next_ref_loc + Vector2(-16,80)
		if next_block.name == "TPiece":
			next_block.position = next_ref_loc + Vector2(16,80)
		if next_block.name == "ZPiece":
			next_block.position = next_ref_loc + Vector2(-16,48)
		next_block.set_visible(true)
	else:
		make_next_bag()
		make_next_bag_debug()
		new_bag += next_bag
		next_block_sprite(new_bag)
	
func make_next_bag():
	randomize()
	var x = 0
	next_bag = []
	while(x < 7):
		var num = randi() % 7
		if !(next_bag.has(num)):
			next_bag.append(num)
			x += 1
			
func make_next_bag_debug():
	next_bag = [0,0,0,0,0,0,0]

func _Area_Entered(obj,the_node):
	var nodename = the_node.name
	var row_var = get(the_node.get_parent().name) # get let's me pass a string and get the var
	row_var[nodename] = obj
	clear_row(the_node)

	
func _Area_Exited(obj,the_node):
	var nodename = the_node.name
	var row_var = get(the_node.get_parent().name) # get let's me pass a string and get the var
	row_var[nodename] = ""

func clear_row(the_node):
	var x = 0
	var nodename = the_node.name
	var row_var = get(the_node.get_parent().name) # get let's me pass a string and get the var

	for row_key in row_var.keys():
		if typeof(row_var[row_key]) == TYPE_STRING:
			break
		else:
			x += 1
			if x == 10:
				for block in row_var.keys():
					row_var[block].queue_free()
					row_var[block] = ""